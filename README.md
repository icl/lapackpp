
     |      \   _ \  \   __| |  /    |     |
     |     _ \  __/ _ \ (      <  __ __|__ __|
    ____|_/  _\_| _/  _\___|_|\_\   _|    _|

**C++ API for the Linear Algebra PACKage**

**Innovative Computing Laboratory**

**University of Tennessee**

LAPACK++ has moved to <https://github.com/icl-utk-edu/lapackpp>
